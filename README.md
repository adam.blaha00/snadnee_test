installation and setup

1. $ git clone https://gitlab.com/adam.blaha00/snadnee_test.git (or via ssh)
2. $ cp .env.example .env
3. $ composer install
4. $ php artisan key:generate
5. You must edit database configuration in .env and create it
6. $ php artisan migrate --seed
7. $ php artisan storage:link   
8. $ php artisan serve

base url http://127.0.0.1:8000/ is redirected to /products
