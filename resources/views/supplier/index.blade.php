@extends('layouts.admin')

@php($mPage = 'box')
@php($page = 'boxIndex')

@section('body')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/English.json"
                },

                responsive: true,
                dom: 'Bfrtip',
                buttons: [
                    {
                    }
                ]
            } );
        } );
    </script>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    @if(Session::has('error_active'))
                        <div class="card p-5">
                            <h4><strong style="color: red;">{{Session::get('error_active')}}</strong></h4>
                        </div>
                    @endif

                    <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">crop</i>
                            </div>
                            <h4 class="card-title">Přehled dodavatelů </h4>
                        </div>
                        <div class="card-body">

                            <div class="toolbar text-right">
                            </div>
                            <div class="table-responsive">
                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    <tr class="text-center">
                                        <th style="width: 15%">ID</th>
                                        <th style="width: 15%">Jméno</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($suppliers as $supplier)
                                        <tr class="text-center">
                                            <td>{{$supplier->id}}</td>
                                            <td>{{$supplier->name}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <div class="col-md-12">
                                {{ $suppliers->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
