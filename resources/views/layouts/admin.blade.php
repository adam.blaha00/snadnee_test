<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'ReasonToSee') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="{{asset('admin/css/material-dashboard.css')}}" rel="stylesheet" />
    <link href="{{asset('admin/css/main.css')}}?v=1.1" rel="stylesheet" />


    <!-- Fav icons  -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favs/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favs/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favs/favicon-16x16.png')}}">

    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <style>
        .nav-pills .nav-item .nav-link.active { background: #1F82C4!important; }
        .btn { padding: 8px 12px; }
        table tbody {
            text-align: center;
        }
        .btn-shadow {
            -webkit-box-shadow: 0px 10px 18px -9px rgba(0,0,0,0.63);
            -moz-box-shadow: 0px 10px 18px -9px rgba(0,0,0,0.63);
            box-shadow: 0px 10px 18px -9px rgba(0,0,0,0.63);
        }
        .dataTables_info {
            display: none;
        }
        .paginate_button {
            display: none;
        }
    </style>
</head>
<body>

<!-- Pre lader  -->
<div id="preloader">
    <div class="centerfy">
        <div class="text-center position-relative" id="here">
            <div class="spiner"></div>
        </div>
    </div>
</div>

<div class="wrapper">
    @include('includes/admin-sidebar')

    <div class="main-panel">
        @include('includes/admin-topbar')
        @yield('body')
        @include('includes/admin-footer')
    </div>

</div>

<!--   Core JS Files   -->
<script src="{{asset('admin/js/core/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>

@yield('datatables')

<script src="{{asset('admin/js/plugins/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
<!-- Plugin for the momentJs  -->
<script src="{{asset('admin/js/plugins/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/plugins/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/plugins/bootstrap-selectpicker.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/plugins/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/js/plugins/bootstrap-notify.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

<script src="{{asset('admin/js/material-dashboard.min.js?v=2.0.2')}}" type="text/javascript"></script>

<script>
    $(document).ready(function() {
        $('#preloader').hide();
        $(':text[title]').tooltip({
            placement: "right",
            trigger: "focus"
        });
    });

    if( $('#loading-form').submit(function(){
        $('#preloader').show();
        var node = document.createElement("h3");
        node.classList.add("generating");
        var textnode = document.createTextNode("Generating schedule");
        node.appendChild(textnode);
        document.getElementById("here").appendChild(node);
    })) {

    }
</script>
<script>
    function setFormValidation(id) {
        $(id).validate({
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
                $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
                $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
            },
            errorPlacement: function(error, element) {
                $(element).append(error);
            },
        });
    }
    $(document).ready(function() {
        setFormValidation('#TypeValidation');
        setFormValidation('#RangeValidation');
    });
</script>


@if(Session::has('info'))
    <script>
        showNotification('top','right');
        function showNotification(from, align){
            $.notify({
                icon: "add_alert",
                message: "{{Session::get('info')}}"
            },{
                type: 'info',
                timer: 2000,
                placement: {
                    from: from,
                    align: align
                }
            });
        }
    </script>
@endif

@if(Session::has('success'))
    <script>
        showNotification('top','right');
        function showNotification(from, align){
            $.notify({
                icon: "add_alert",
                message: "{{Session::get('success')}}"
            },{
                type: 'success',
                timer: 2000,
                placement: {
                    from: from,
                    align: align
                }
            });
        }
    </script>
@endif

@if(Session::has('destroy'))
    <script>
        showNotification('top','right');
        function showNotification(from, align){
            $.notify({
                icon: "add_alert",
                message: "{{Session::get('destroy')}}"
            },{
                type: 'danger',
                timer: 200,
                placement: {
                    from: from,
                    align: align
                }
            });
        }
    </script>
@endif


@yield('script')

</body>

</html>


