<footer class="footer">
    <div class="container-fluid">
        <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>
            <a href="https://webforyou.eu/" target="_blank">WebForYou
            </a>
        </div>
    </div>
</footer>