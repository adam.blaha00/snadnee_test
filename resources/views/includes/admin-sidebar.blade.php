<div class="sidebar" data-color="rose" data-background-color="black">
    <div class="logo text-center">
        <a href="{{route('products.index')}}" class="simple-text logo-normal">
            SNADNEE
        </a>
    </div>
    <div class="sidebar-wrapper">

        <ul class="nav">
            <li class="nav-item {{$mPage == 'product' ? 'active' : ''}} ">
                <a class="nav-link" data-toggle="collapse" href="#product">
                    <i class="material-icons">crop</i>
                    <p> Produkty
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{$mPage == 'product' ? 'show' : ''}}" id="product">
                    <ul class="nav">
                        <li class="nav-item {{$page == 'productIndex' ? 'active' : ''}}">
                            <a class="nav-link" href="{{route('products.index')}}">
                                <i class="material-icons">
                                    play_arrow
                                </i>
                                <span class="sidebar-normal"> Přehled produktů</span>
                            </a>
                        </li>
                        <li class="nav-item {{$page == 'productCreate' ? 'active' : ''}}">
                            <a class="nav-link" href="{{route('products.create')}}">
                                <i class="material-icons">
                                    play_arrow
                                </i>
                                <span class="sidebar-normal"> Vytvořit nový produkt </span>
                            </a>
                        </li>
                        @if($page == 'productEdit')
                            <li class="nav-item {{$page == 'productEdit' ? 'active' : ''}}">
                                <a class="nav-link" href="{{route('products.edit', $product->id)}}">
                                    <i class="material-icons">
                                        play_arrow
                                    </i>
                                    <span class="sidebar-normal"> Úprava produktu </span>
                                </a>
                            </li>
                        @endif
                        @if($page == 'productShow')
                            <li class="nav-item {{$page == 'productShow' ? 'active' : ''}}">
                                <a class="nav-link" href="{{route('products.show', $product->id)}}">
                                    <i class="material-icons">
                                        play_arrow
                                    </i>
                                    <span class="sidebar-normal"> Zobrazení produktu </span>
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </li>
            <li class="nav-item {{$mPage == 'box' ? 'active' : ''}} ">
                <a class="nav-link" data-toggle="collapse" href="#box">
                    <i class="material-icons">crop</i>
                    <p> Bedýnky
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{$mPage == 'box' ? 'show' : ''}}" id="box">
                    <ul class="nav">
                        <li class="nav-item {{$page == 'boxIndex' ? 'active' : ''}}">
                            <a class="nav-link" href="{{route('boxes.index')}}">
                                <i class="material-icons">
                                    play_arrow
                                </i>
                                <span class="sidebar-normal"> Přehled bedýnek </span>
                            </a>
                        </li>
                        <li class="nav-item {{$page == 'boxCreate' ? 'active' : ''}}">
                            <a class="nav-link" href="{{route('boxes.create')}}">
                                <i class="material-icons">
                                    play_arrow
                                </i>
                                <span class="sidebar-normal"> Vytvořit bedínku </span>
                            </a>
                        </li>
                        @if($page == 'boxEdit')
                            <li class="nav-item {{$page == 'boxEdit' ? 'active' : ''}}">
                                <a class="nav-link" href="{{route('boxes.edit', $box->id)}}">
                                    <i class="material-icons">
                                        play_arrow
                                    </i>
                                    <span class="sidebar-normal">Úprava bedínky</span>
                                </a>
                            </li>
                        @endif
                        @if($page == 'boxShow')
                            <li class="nav-item {{$page == 'boxShow' ? 'active' : ''}}">
                                <a class="nav-link" href="{{route('boxes.show', $box->id)}}">
                                    <i class="material-icons">
                                        play_arrow
                                    </i>
                                    <span class="sidebar-normal">Zobrazení bedínky</span>
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </li>
            <li class="nav-item {{$mPage == 'supplier' ? 'active' : ''}} ">
                <a class="nav-link" data-toggle="collapse" href="#supplier">
                    <i class="material-icons">crop</i>
                    <p> Dodavatelé
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{$mPage == 'supplier' ? 'show' : ''}}" id="supplier">
                    <ul class="nav">
                        <li class="nav-item {{$page == 'supplierIndex' ? 'active' : ''}}">
                            <a class="nav-link" href="{{route('suppliers.index')}}">
                                <i class="material-icons">
                                    play_arrow
                                </i>
                                <span class="sidebar-normal"> Přehled dodavatelů </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
