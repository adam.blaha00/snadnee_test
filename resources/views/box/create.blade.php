@extends('layouts.admin')
@php($mPage = 'box')
@php($page = 'boxCreate')

@section('body')

    <style>
        .tooltip-inner {
            text-align: left;
            white-space:pre;
            max-width: none;
        }
    </style>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto">
                    {!! Form::open(['method'=>'POST', 'class'=>'form-horizontal form-label-left', 'action'=> 'App\Http\Controllers\BoxController@store']) !!}
                    <div class="card ">
                        <div class="card-header card-header-primary card-header-text">
                            <div class="card-text">
                                <h4 class="card-title">Vytváření nové bedínky</h4>
                            </div>
                        </div>
                        <div class="card-body ">

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Sleva</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <input class="form-control" type="number" step="0.1" name="sale" value="{{old('sale')}}" required placeholder="Zadejte slevu boxu"/>
                                        @if ($errors->has('name'))
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Dodavatel {{old('supplier_id')}}</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <select
                                            class="selectpicker form-control"
                                            data-style="select-with-transition"
                                            data-size="10"
                                            name="products[]"
                                            multiple
                                            title="Vyberte produkty"
                                            required>
                                            @foreach($products as $id => $name)
                                                <option value="{{$id}}" {{(int) old('products') === $id ? 'selected' : ''}}>{{$name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('products'))
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('supplier_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer ml-auto mr-auto">
                                <div class="col  text-center">
                                    <button type="submit" class="btn btn-lg btn-success">Save</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
