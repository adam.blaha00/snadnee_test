@extends('layouts.admin')
@php($mPage = 'box')
@php($page = 'boxEdit')

@section('body')

    <style>
        .tooltip-inner {
            text-align: left;
            white-space:pre;
            max-width: none;
        }
    </style>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto">
                    {!! Form::model($box, ['method'=>'PATCH', 'class'=>'form-horizontal form-label-left', 'action'=> ['App\Http\Controllers\BoxController@update', $box->id]]) !!}
                    <div class="card ">
                        <div class="card-header card-header-primary card-header-text">
                            <div class="card-text">
                                <h4 class="card-title">Úprava bedínky</h4>
                            </div>
                        </div>
                        <div class="card-body ">

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Sleva</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <input class="form-control" type="number" step="0.1" name="sale" value="{{$box->sale}}" required placeholder="Zadejte slevu boxu"/>
                                        @if ($errors->has('sale'))
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('sale') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            @php($selectedProductIds = $box->products()->pluck('products.id')->toArray())

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Produkty v boxu</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <select class="selectpicker form-control"
                                                data-style="select-with-transition"
                                                data-live-search="true"
                                                data-size="15"
                                                name="products[]"
                                                multiple
                                                title="Vyberte produkty v boxu"
                                                required
                                        >
                                            @foreach($products as $id => $name)
                                                <option value="{{$id}}" {{in_array($id, $selectedProductIds, true) ? 'selected' : ''}}>{{$name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('products'))
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('products') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer ml-auto mr-auto">
                                <div class="col  text-center">
                                    <button type="submit" class="btn btn-lg btn-success">Save</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
