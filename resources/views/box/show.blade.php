@extends('layouts.admin')
@php($mPage = 'box')
@php($page = 'boxShow')

@section('body')

    <style>
        .tooltip-inner {
            text-align: left;
            white-space:pre;
            max-width: none;
        }
    </style>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto">
                    <div class="card ">
                        <div class="card-header card-header-primary card-header-text">
                            <div class="card-text">
                                <h4 class="card-title">Zobrazení bedínky</h4>
                            </div>
                        </div>
                        <div class="card-body ">

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Sleva</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <input class="form-control" type="number" step="0.1" name="sale" value="{{$box->sale}}" disabled/>
                                        @if ($errors->has('sale'))
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('sale') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Produkty v boxu</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <select class="selectpicker form-control"
                                                data-style="select-with-transition"
                                                data-live-search="true"
                                                data-size="15"
                                                name="products[]"
                                                multiple
                                                disabled
                                        >
                                            @foreach($box->products as $product)
                                                <option value="{{$product->id}}" selected>{{$product->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
