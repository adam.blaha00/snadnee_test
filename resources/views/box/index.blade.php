@extends('layouts.admin')

@php($mPage = 'box')
@php($page = 'boxIndex')

@section('body')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/English.json"
                },

                responsive: true,
                dom: 'Bfrtip',
                buttons: [
                    {
                    }
                ]
            } );
        } );
    </script>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    @if(Session::has('error_active'))
                        <div class="card p-5">
                            <h4><strong style="color: red;">{{Session::get('error_active')}}</strong></h4>
                        </div>
                    @endif

                    <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">crop</i>
                            </div>
                            <h4 class="card-title">Přehled boxů </h4>
                        </div>
                        <div class="card-body">

                            <div class="toolbar text-right">
                                <a href="{{route('boxes.create')}}" class="btn btn-success">Vytvořit nový box</a>
                            </div>
                            <div class="table-responsive">
                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    <tr class="text-center">
                                        <th style="width: 15%">ID</th>
                                        <th style="width: 15%">Počet produktů</th>
                                        <th style="width: 15%">Sleva</th>
                                        <th style="width: 10%" class="disabled-sorting ">Akce</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($boxes as $box)
                                        <tr class="text-center">
                                            <td>{{$box->id}}</td>
                                            <td>{{$box->products->count()}}</td>
                                            <td>{{$box->sale}}</td>
                                            <td>
                                                <a href="{{route('boxes.edit', $box->id)}}" class="btn btn-warning btn-xs">Upravit box </a>
                                                <a href="{{route('boxes.show', $box->id)}}" class="btn btn-info btn-xs">Zobrazit box </a>
                                                <a href="#"
                                                   class="btn btn-danger delete"
                                                   onclick="clicked({{$box->id}});">
                                                    <i class="material-icons">delete</i>
                                                    Smazat box
                                                </a>

                                                {!! Form::open(['method'=>'DELETE', 'class'=>'delete', 'id'=>'delete-form-'.$box->id, 'style'=>'display:none;',  'action'=> ['App\Http\Controllers\BoxController@destroy', $box->id]]) !!}
                                                {!! Form::button('DELETE', ['type' => 'submit', 'class'=>'btn btn-danger remove']) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <div class="col-md-12">
                                {{ $boxes->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function clicked(id) {
            if (confirm('Are you sure you want to delete item |id - '+id+'|?')) {
                document.getElementById('delete-form-'+id).submit();
            } else {
                return false;
            }
        }
    </script>

@stop
