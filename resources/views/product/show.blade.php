@extends('layouts.admin')
@php($mPage = 'product')
@php($page = 'productShow')

@section('body')

    <style>
        .tooltip-inner {
            text-align: left;
            white-space:pre;
            max-width: none;
        }
    </style>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto">
                    <div class="card ">
                        <div class="card-header card-header-primary card-header-text">
                            <div class="card-text">
                                <h4 class="card-title">Zobrazení produktu</h4>
                            </div>
                        </div>
                        <div class="card-body ">

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Název</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="name" value="{{$product->name}}" disabled placeholder="Zadejte název produktu"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label label-checkbox">Je product skladem?</label>
                                <div class="col-sm-10 checkbox-radios">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="in_stock" value="1" {{$product->in_stock === 1 ? 'checked' : ''}} disabled> Ano
                                            <span class="circle">
                                              <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="in_stock" value="0" {{$product->in_stock === 0 ? 'checked' : ''}} disabled> Ne
                                            <span class="circle">
                                              <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Cena</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <input class="form-control" type="number" step=".01" name="price" value="{{$product->price}}" disabled placeholder="Zadejte cenu produktu"/>
                                    </div>
                                </div>
                            </div>

                            @if($product->file)
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Fotografie</label>
                                    <div class="col-sm-7">
                                        <div class="form-group mt-3">
                                            <div>
                                                <img src="{{asset($product->file)}}" alt="" style="max-width: 300px;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Dodavatel</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <select class="selectpicker form-control"
                                                data-style="select-with-transition"
                                                data-live-search="true"
                                                data-size="15"
                                                name="supplier_id"
                                                title="Vyberte dodavatele"
                                                disabled
                                        >
                                            <option value="#" selected>{{$product->supplier->name}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Bedýnky</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <select class="selectpicker form-control"
                                                data-style="select-with-transition"
                                                data-live-search="true"
                                                data-size="15"
                                                name="boxes[]"
                                                multiple
                                                title="Vyberte produkty v boxu"
                                                disabled
                                        >
                                            @foreach($product->boxes as $box)
                                                <option value="{{$box->id}}" selected>{{$box->id}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
