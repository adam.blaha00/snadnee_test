@extends('layouts.admin')
@php($mPage = 'product')
@php($page = 'productEdit')

@section('body')

    <style>
        .tooltip-inner {
            text-align: left;
            white-space:pre;
            max-width: none;
        }
    </style>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto">
                    {!! Form::model($product, ['method'=>'PATCH', 'class'=>'form-horizontal form-label-left', 'action'=> ['App\Http\Controllers\ProductController@update', $product->id], 'files' => true]) !!}
                    <div class="card ">
                        <div class="card-header card-header-primary card-header-text">
                            <div class="card-text">
                                <h4 class="card-title">Vytváření nového produktu</h4>
                            </div>
                        </div>
                        <div class="card-body ">

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Název</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="name" value="{{$product->name}}" required placeholder="Zadejte název produktu"/>
                                        @if ($errors->has('name'))
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label label-checkbox">Je product skladem?</label>
                                <div class="col-sm-10 checkbox-radios">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="in_stock" value="1" {{$product->in_stock === 1 ? 'checked' : ''}}> Ano
                                            <span class="circle">
                                              <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="in_stock" value="0" {{$product->in_stock === 0 ? 'checked' : ''}}> Ne
                                            <span class="circle">
                                              <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    @if ($errors->has('in_stock'))
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('in_stock') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Cena</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <input class="form-control" type="number" step=".01" name="price" value="{{$product->price}}" required placeholder="Zadejte cenu produktu"/>
                                        @if ($errors->has('price'))
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Fotografie</label>
                                <div class="col-sm-7">
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput" style="padding-top: 10px;">
                                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                        <div>
                                            <input type="file" name="file" />
                                        </div>
                                        @if ($errors->has('file'))
                                            <span class="text-danger" role="alert">
                                            <strong>{{ $errors->first('file') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Dodavatel</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <select class="selectpicker form-control"
                                                data-style="select-with-transition"
                                                data-live-search="true"
                                                data-size="15"
                                                name="supplier_id"
                                                title="Vyberte dodavatele"
                                                required
                                        >
                                            @foreach($suppliers as $id => $name)
                                                <option value="{{$id}}" {{$product->supplier->id === $id ? 'selected' : ''}}>{{$name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('supplier_id'))
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('supplier_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            @php($selectedBoxIds = $product->boxes()->pluck('boxes.id')->toArray())

                            <div class="row">
                                <label class="col-sm-2 col-form-label">Bedýnky</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <select class="selectpicker form-control"
                                                data-style="select-with-transition"
                                                data-live-search="true"
                                                data-size="15"
                                                name="boxes[]"
                                                multiple
                                                title="Vyberte produkty v boxu"
                                        >
                                            @foreach($boxes as $id)
                                                <option value="{{$id}}" {{in_array($id, $selectedBoxIds, true) ? 'selected' : ''}}>{{$id}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('boxes'))
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('boxes') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer ml-auto mr-auto">
                                <div class="col  text-center">
                                    <button type="submit" class="btn btn-lg btn-success">Save</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
