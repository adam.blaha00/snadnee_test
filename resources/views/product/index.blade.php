@extends('layouts.admin')

@php($mPage = 'product')
@php($page = 'productIndex')

@section('body')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/English.json"
                },

                responsive: true,
                dom: 'Bfrtip',
                buttons: [
                    {
                    }
                ]
            } );
        } );
    </script>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    @if(Session::has('error_active'))
                        <div class="card p-5">
                            <h4><strong style="color: red;">{{Session::get('error_active')}}</strong></h4>
                        </div>
                    @endif

                    <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">crop</i>
                            </div>
                            <h4 class="card-title">OVERVIEW PRODUCTS </h4>
                        </div>
                        <div class="card-body">

                            <form action="" method="GET" id="filter">
                                @csrf
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Id</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="id" value="{{Request::get('id')}}" placeholder="Zadejte id produktu"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Název</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="name" value="{{Request::get('name')}}" placeholder="Zadejte název produktu"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label label-checkbox">Je product skladem?</label>
                                    <div class="col-sm-10 checkbox-radios">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="in_stock" value="1" {{(int) Request::get('in_stock') === 1 ? 'checked' : ''}}> Ano
                                                <span class="circle">
                                              <span class="check"></span>
                                            </span>
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="in_stock" value="0" {{(int) Request::get('in_stock') === 0 ? 'checked' : ''}}> Ne
                                                <span class="circle">
                                              <span class="check"></span>
                                            </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Dodavatel</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <select class="selectpicker form-control"
                                                    data-style="select-with-transition"
                                                    data-live-search="true"
                                                    data-size="15"
                                                    name="supplier_ids[]"
                                                    multiple
                                                    title="Vyberte dodavatele"
                                            >
                                                @foreach($suppliers as $id => $name)
                                                    <option value="{{$id}}" {{ isset(array_flip((Request::get('supplier_ids') ?? []))[$id]) ? 'selected' : ''}}>{{$name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-footer ml-auto mr-auto">
                                    <div class="col  text-center">
                                        <button type="submit" form="filter" class="btn btn-sm btn-success">Hledat</button>
                                    </div>
                                </div>
                            </form>

                            <form action="" id="sorting_form">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Seřadit podle</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <select class="selectpicker form-control"
                                                    data-style="select-with-transition"
                                                    name="sorting_by"
                                                    title="Vyberte podle čeho se má tabulka seřadit"
                                                    required
                                            >
                                                <option value="id" {{ Request::get('sorting_by') === 'id' ? 'selected' : ''}}>Id</option>
                                                <option value="updated_at" {{ Request::get('sorting_by') === 'updated_at' ? 'selected' : ''}}>Poslední čas editace</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Typ řazení</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <select class="selectpicker form-control"
                                                    data-style="select-with-transition"
                                                    name="sorting_type"
                                                    title="Vyberte typ řazení"
                                                    required
                                            >
                                                <option value="asc" {{ Request::get('sorting_type') === 'asc' ? 'selected' : ''}}>Vzestupně</option>
                                                <option value="desc" {{ Request::get('sorting_type') === 'desc' ? 'selected' : ''}}>Sestupně</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer ml-auto mr-auto">
                                    <div class="col  text-center">
                                        <button type="submit" form="sorting_form" class="btn btn-sm btn-success">Seřadit</button>
                                    </div>
                                </div>
                            </form>

                            <div class="toolbar text-right">
                                <a href="{{route('products.create')}}" class="btn btn-success">Vytvořit nový produkt</a>
                            </div>
                            <div class="table-responsive">
                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    <tr class="text-center">
                                        <th style="width: 15%">ID</th>
                                        <th style="width: 15%">Název</th>
                                        <th style="width: 15%">Skladem</th>
                                        <th style="width: 15%">Dodavatel</th>
                                        <th style="width: 10%" class="disabled-sorting ">Akce</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($products as $product)
                                        <tr class="text-center">
                                            <td>{{$product->id}}</td>
                                            <td>{{$product->name}}</td>
                                            <td>{{$product->in_stock === 1 ? 'Ano' : 'Ne'}}</td>
                                            <td>{{$product->supplier->name}}</td>
                                            <td>
                                                <a href="{{route('products.edit', $product->id)}}" class="btn btn-warning btn-xs">Upravit produkt </a>
                                                <a href="{{route('products.show', $product->id)}}" class="btn btn-info btn-xs">Zobrazit produkt </a>
                                                <a href="#"
                                                   class="btn btn-danger delete"
                                                   onclick="clicked({{$product->id}});">
                                                    <i class="material-icons">delete</i>
                                                    Smazat produkt
                                                </a>

                                                {!! Form::open(['method'=>'DELETE', 'class'=>'delete', 'id'=>'delete-form-'.$product->id, 'style'=>'display:none;',  'action'=> ['App\Http\Controllers\ProductController@destroy', $product->id]]) !!}
                                                {!! Form::button('DELETE', ['type' => 'submit', 'class'=>'btn btn-danger remove']) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <div class="col-md-12">
                                {{ $products->appends(['id' => Request::get('id'), 'name' => Request::get('name'), 'in_stock' => Request::get('in_stock'), 'supplier_ids' => Request::get('supplier_ids')]) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function clicked(id) {
            if (confirm('Are you sure you want to delete item |id - '+id+'|?')) {
                document.getElementById('delete-form-'+id).submit();
            } else {
                return false;
            }
        }
    </script>

@stop
