<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductBoxTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_box_tags', function (Blueprint $table) {
            $table->id();
            $table->foreignId('box_id');
            $table->foreignId('product_id');
            $table->timestamps();

            $table->foreign('box_id')->references('id')->on('boxes');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_box_tags');
    }
}
