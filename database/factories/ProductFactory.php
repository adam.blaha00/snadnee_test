<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'in_stock' => $this->faker->boolean(),
            'price' => $this->faker->numberBetween(10, 1000),
            'supplier_id' => Supplier::query()->inRandomOrder()->first()->id,
        ];
    }
}
