<?php

namespace Database\Seeders;

use App\Models\Box;
use App\Models\Product;
use Illuminate\Database\Seeder;

class BoxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Box::factory()->count(20)->create();

        foreach (Box::all() as $box) {
            $products = Product::query()->inRandomOrder()->take(5)->pluck('id')->toArray();

            $box->products()->sync($products);
        }
    }
}
