<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string|max:191',
            'in_stock' => 'nullable|boolean',
            'price' => 'required|numeric',
            'file' => 'nullable|mimes:jpg,jpeg,png',
            'supplier_id' => 'required|integer',
            'boxes' => 'nullable|array'
        ];
    }
}
