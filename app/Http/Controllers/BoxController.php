<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Contracts\Repositories\BoxRepository;
use App\Http\Requests\BoxRequest;
use App\Models\Product;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class BoxController extends Controller
{
    private BoxRepository $boxRepository;

    public function __construct(BoxRepository $boxRepository)
    {
        $this->boxRepository = $boxRepository;
    }

    public function index(): View
    {
        $boxes = $this->boxRepository->getPaginatedBoxes();

        return view('box.index', compact('boxes'));
    }

    public function create(): View
    {
        $products = Product::query()->pluck('name', 'id');
        return view('box.create', compact('products'));
    }

    public function store(BoxRequest $request): RedirectResponse
    {
        $this->boxRepository->save($request);

        return redirect()->route('boxes.index');
    }

    public function show(int $id): View
    {
        $box = $this->boxRepository->findOneWithProductsById($id);

        return view('box.show', compact('box'));
    }

    public function edit(int $id): View
    {
        [$box, $products] = $this->getEditData($id);

        return view('box.edit', compact('box', 'products'));
    }

    public function update(BoxRequest $request, int $id): RedirectResponse
    {
        $this->boxRepository->save($request, $id);

        return redirect()->route('boxes.index');
    }

    public function destroy(int $id): RedirectResponse
    {
        $this->boxRepository->delete($id);

        return back();
    }

    private function getEditData(int $id): array
    {
        $box = $this->boxRepository->findOneWithProductsById($id);
        $products = Product::query()->pluck('name', 'id');

        return [$box, $products];
    }
}
