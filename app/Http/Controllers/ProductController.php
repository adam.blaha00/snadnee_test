<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Box;
use App\Models\Supplier;
use App\Repositories\ProductRepository;
use Illuminate\Contracts\View\View;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index(Request $request): View
    {
        $products = $this->productRepository->getTableData($request);
        $suppliers = Supplier::query()->pluck('name', 'id');

        return view('product.index', compact('products', 'suppliers'));
    }

    public function create(): View
    {
        $suppliers = Supplier::query()->pluck('name', 'id');
        $boxes = Box::query()->pluck('id');

        return view('product.create', compact('suppliers', 'boxes'));
    }

    public function store(ProductRequest $request): RedirectResponse
    {
        $this->productRepository->save($request);

        return redirect()->route('products.index');
    }

    public function show(int $id): View
    {
        $product = $this->productRepository->findOneById($id);

        return view('product.show', compact('product'));
    }

    public function edit(int $id): View
    {
        [$product, $suppliers, $boxes] = $this->getEditData($id);

        return view('product.edit', compact('product', 'suppliers', 'boxes'));
    }

    public function update(ProductRequest $request, int $id): RedirectResponse
    {
        $this->productRepository->save($request, $id);

        return redirect()->route('products.index');
    }

    public function destroy(int $id): RedirectResponse
    {
        $this->productRepository->delete($id);

        return back();
    }

    private function getEditData(int $id): array
    {
        $product = $this->productRepository->findOneById($id);
        $suppliers = Supplier::query()->pluck('name', 'id');
        $boxes = Box::query()->pluck('id');

        return [$product, $suppliers, $boxes];
    }
}
