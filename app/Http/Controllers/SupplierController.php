<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Supplier;

class SupplierController extends Controller
{
    public function index()
    {
        $suppliers = Supplier::query()->paginate(10);

        return view('supplier.index', compact('suppliers'));
    }
}
