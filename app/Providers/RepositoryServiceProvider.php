<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        foreach ($this->repositories() as $repositoryContract => $repositoryClass) {
            $this->app->bind($repositoryContract, $repositoryClass);
        }
    }

    private function repositories(): array
    {
        return [
            \App\Contracts\Repositories\BoxRepository::class => \App\Repositories\BoxRepository::class,
            \App\Contracts\Repositories\ProductRepository::class => \App\Repositories\ProductRepository::class
        ];
    }
}
