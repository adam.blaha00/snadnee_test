<?php

declare(strict_types=1);

namespace App\Tools;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class Helper
{
    public static function getFileNameAndUploadFile(UploadedFile $fileFormRequest, string $model): string
    {
        $fileName = time() . '.'. $fileFormRequest->getClientOriginalExtension();

        Storage::disk('public')->putFileAs($model::FILE_PATH, $fileFormRequest, $fileName);

        return $fileName;
    }
}
