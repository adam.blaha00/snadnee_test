<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Product;
use App\Models\Supplier;
use App\Tools\Helper;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Contracts\Repositories\ProductRepository as ProductRepositoryContract;
use Illuminate\Http\Request;

class ProductRepository implements ProductRepositoryContract
{
    public function getTableData(Request $request): LengthAwarePaginator
    {
        return Product::query()->tableFilter($request->all())->orderBy($request->sorting_by ?? 'id', $request->sorting_type ?? 'asc')->paginate(10);
    }

    public function findOneById(int $productId): ?Product
    {
        return Product::query()->findOrFail($productId);
    }

    public function save(Request $request, ?int $id = null): void
    {
        $product = $this->getCreatedOrUpdatedProduct($request, $id);

        if($supplier = Supplier::query()->where('id', '=', $request->supplier_id)->first()){
            $product->supplier()->associate($supplier);
        }
        $product->boxes()->sync($request->boxes);
        $product->save();
    }

    public function delete(int $id): void
    {
        Product::query()->findOrFail($id)->delete();
    }

    private function getCreatedOrUpdatedProduct(Request $request, ?int $id): Product
    {
        $product = Product::query()->updateOrCreate(
            ['id' => $id],
            $this->getProductData($request)
        );

        if(!$file = $request->file('file')){
            return $product;
        }

        $product->file = Helper::getFileNameAndUploadFile($file, Product::class);

        return $product;
    }

    private function getProductData(Request $request): array
    {
        return [
            'name' => $request->name,
            'in_stock' => (int) $request->in_stock,
            'price' => $request->price,
        ];
    }
}
