<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Box;
use \App\Contracts\Repositories\BoxRepository as BoxRepositoryContract;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class BoxRepository implements BoxRepositoryContract
{
    public function getPaginatedBoxes(): LengthAwarePaginator
    {
        return Box::query()->paginate(10);
    }

    public function findOneById(int $boxId): ?Box
    {
        return Box::query()->findOrFail($boxId);
    }

    public function findOneWithProductsById(int $boxId): ?Box
    {
        return Box::query()->find($boxId)->with('products')->firstOrFail();
    }

    public function save(Request $request, ?int $id = null): void
    {
        $box = $this->getCreatedOrUpdatedBox($request, $id);

        $box->products()->sync($request->products);
    }

    public function delete(int $id): void
    {
        Box::query()->findOrFail($id)->delete();
    }

    private function getCreatedOrUpdatedBox(Request $request, ?int $id): Box
    {
        return Box::query()->updateOrCreate(
            ['id' => $id],
            $this->getBoxData($request)
        );
    }

    private function getBoxData(Request $request): array
    {
        return [
            'sale' => $request->sale,
        ];
    }
}
