<?php

declare(strict_types=1);

namespace App\Contracts\Repositories;

use App\Models\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

interface ProductRepository
{
    public function getTableData(Request $request): LengthAwarePaginator;

    public function findOneById(int $productId): ?Product;

    public function save(Request $request, ?int $id = null): void;

    public function delete(int $id): void;
}
