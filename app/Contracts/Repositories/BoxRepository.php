<?php

declare(strict_types=1);

namespace App\Contracts\Repositories;

use App\Models\Box;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

interface BoxRepository
{
    public function getPaginatedBoxes(): LengthAwarePaginator;

    public function findOneById(int $boxId): ?Box;

    public function findOneWithProductsById(int $boxId): ?Box;

    public function save(Request $request, ?int $id = null): void;

    public function delete(int $id): void;
}
