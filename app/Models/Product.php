<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    private const CURRENCY_CONVERT_CONSTANT = 100;
    public const FILE_PATH = 'product_img/';

    protected $fillable = [
        'name',
        'in_stock',
        'price',
        'file',
        'supplier_id'
    ];

    public function scopeTableFilter($query, array $searched): void
    {
        if(isset($searched['id'])){
            // laravel alternative $query->whereId($searched['id'])
            // I usually use the variant below
            $query->where('id', '=', $searched['id']);
        }

        if(isset($searched['name'])){
            $query->where('name', '=', $searched['name']);
        }

        if(isset($searched['in_stock'])){
            $query->where('in_stock', '=', $searched['in_stock']);
        }

        if(isset($searched['supplier_ids'])){
            $query->whereIn('id', $searched['supplier_ids']);
        }
    }

    public function getFileAttribute(?string $file): ?string
    {
        if(is_null($file)) {
            return null;
        }

        return Storage::disk('public')->url(self::FILE_PATH . $file);
    }

    public function setPriceAttribute(float $value): void
    {
        $this->attributes['price'] = $value*self::CURRENCY_CONVERT_CONSTANT;
    }

    public function getPriceAttribute(int $price): float
    {
        return $price/self::CURRENCY_CONVERT_CONSTANT;
    }

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }

    public function boxes(): BelongsToMany
    {
        return $this->belongsToMany(Box::class, 'product_box_tags');
    }
}
